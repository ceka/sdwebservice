package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;


//The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

 @Inject
 private Logger log;

 @Inject
 private AsignaturaDAO dao;

 public void crear(Asignatura p) throws Exception {
     log.info("Creando Asignatura: " + p.getNombre());
     try {
     	dao.insertar(p);
     }catch(Exception e) {
     	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
     	throw e;
     }
     log.info("Asignatura creada con éxito: " + p.getNombre() );
 }
 
 public List<Asignatura> seleccionar() {
 	return dao.seleccionar();
 }
 
 public long actualizar(Asignatura p) throws Exception{
	 return dao.actualizar(p);
 }
 
 public long borrar(long numeroId) throws Exception {
 	return dao.borrar(numeroId);
 }
 
 public Asignatura seleccionarPorId(long numeroId) {
 	return dao.seleccionarPorId(numeroId);
 }
 
 public List<Asignatura> listarAsignaturas(long personaId)throws Exception{
	 return dao.listarAsignaturas(personaId);
 }
 
 public List<Persona> listarPersonas(long asignaturaId)throws Exception{
	 return dao.listarPersonas(asignaturaId);
 }
 
 public long asociar(Persona p) throws Exception {
	 return dao.asociar(p);
 }
 
 public long desasociar(Persona p) throws Exception{
	 return dao.desasociar(p);
 }
 
 
}