package py.una.pol.personas.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;




@Stateless
public class AsignaturaDAO {
	@Inject
    private Logger log;
    
	/**
	 * 
	 * @param condiciones 
	 * @return
	 */
	// para listar asignaturas
	public List<Asignatura> seleccionar() {
		String query = "SELECT id, nombre FROM asignatura ";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	// lista todas las personas de una asignatura
		public List<Persona> listarPersonas(long asignaturaId) throws SQLException {
			//String query = "SELECT id, nombre FROM asignatura WHERE id = ?";
			
			String SQL ="SELECT p.cedula, p.nombre, p.apellido FROM persona p " + 
			" JOIN persona_asignatura pa ON p.cedula = pa.id_persona " + 
			" join asignatura a on a.id =pa.id_asignatura  where a.id = ?";
			
			List<Persona> lista = new ArrayList<Persona>();
			
			Connection conn = null; 
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	        	pstmt.setLong(1, asignaturaId);
	        	
	        	ResultSet rs = pstmt.executeQuery();

	        	while(rs.next()) {
	        		Persona p = new Persona();
	        		p.setCedula(rs.getLong(1));
	        		p.setNombre(rs.getString(2));
	        		p.setApellido(rs.getString(3));
	        		lista.add(p);
	        	}
	        	
	        } catch (SQLException ex) {
	            log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return lista;

		}
	
	
	// lista todas las asignaturas de una persona
	public List<Asignatura> listarAsignaturas(long personaId) throws SQLException {
		//String query = "SELECT id, nombre FROM asignatura WHERE id = ?";
		
		String SQL ="SELECT a.id, a.nombre FROM persona p" + 
		" JOIN persona_asignatura pa ON p.cedula = pa.id_persona " + 
		" join asignatura a on a.id =pa.id_asignatura  where p.cedula = ?";
		
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, personaId);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Asignatura p = new Asignatura();
        		p.setId(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		lista.add(p);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	//crear una asignatura
	public long insertar(Asignatura p) throws SQLException {

        String SQL = "INSERT INTO asignatura(id, nombre) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, p.getId());
            pstmt.setString(2, p.getNombre());
            
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        log.severe("valor retornado de id = "+id );	
        return id;
    }
	// se guarda en una tabla persona_asignatura la relacion de una persona este 
	//inscripta en una asignatura 
	public long desasociar(Persona p) throws SQLException {

        String SQL = "delete from persona_asignatura where id_persona = ? and id_asignatura= ?";
        String traerId ="select id from asignatura where nombre= ?";
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	if(p.getAsignaturas().isEmpty() == true)
            	log.severe("No se envio ninguna asignatura ");
            else if(p.getAsignaturas().size() > 1)
            	log.severe("Debe enviar una sola asignatura para asociar ");
            else {
            	long id_asignatura=0;
            	conn = Bd.connect();
            	PreparedStatement pstmt1 = conn.prepareStatement(traerId);
            	String nombre=p.getAsignaturas().get(0);
            	pstmt1.setString(1, nombre);
            	ResultSet rs = pstmt1.executeQuery();
            	try {
            		if(rs.next()) {
                		
                		id_asignatura=rs.getLong(1);
            		}                	
            	} catch (SQLException ex) {
            		log.severe("Error en la seleccion: " + ex.getMessage());
            	}
            	
            	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            	pstmt.setLong(1, p.getCedula());
            	pstmt.setLong(2, id_asignatura);
            
            	int affectedRows = pstmt.executeUpdate();
            	
            	// check the affected rows 
                if (affectedRows > 0) {
                    // get the ID back
                    try (ResultSet rs1 = pstmt.getGeneratedKeys()) {
                        if (rs1.next()) {
                            id = rs1.getLong(1);
                        }
                    } catch (SQLException ex) {
                    	throw ex;
                    }
                }
            	
            }
 
            
            
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    }
	
	// elimina el registro de la tabla que relaciona que una persona este
	//este matriculado en una asignatura
	public long asociar(Persona p) throws SQLException {

        String SQL = "INSERT INTO persona_asignatura(id_persona, id_asignatura) "
                + "VALUES(?,?)";
        
        String traerId ="select id from asignatura where nombre= ?";
        
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	
        	if(p.getAsignaturas().isEmpty() == true)
            	log.severe("No se envio ninguna asignatura ");
            else if(p.getAsignaturas().size() > 1)
            	log.severe("Debe enviar una sola asignatura para asociar ");
            else {
            	long id_asignatura=0;
            	conn = Bd.connect();
            	PreparedStatement pstmt1 = conn.prepareStatement(traerId);
            	String nombre=p.getAsignaturas().get(0);
            	pstmt1.setString(1, nombre);
            	ResultSet rs = pstmt1.executeQuery();
            	try {
            		if(rs.next()) {
                		
                		id_asignatura=rs.getLong(1);
            		}                	
            	} catch (SQLException ex) {
            		log.severe("Error en la seleccion: " + ex.getMessage());
            	}
            	
            	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            	pstmt.setLong(1, p.getCedula());
            	pstmt.setLong(2, id_asignatura);
            
            	int affectedRows = pstmt.executeUpdate();
            	
            	// check the affected rows 
                if (affectedRows > 0) {
                    // get the ID back
                    try (ResultSet rs1 = pstmt.getGeneratedKeys()) {
                        if (rs1.next()) {
                            id = rs1.getLong(1);
                        }
                    } catch (SQLException ex) {
                    	throw ex;
                    }
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;
    }
	
	
	public long actualizar(Asignatura p) throws SQLException {

        String SQL = "UPDATE asignatura SET nombre = ?  WHERE id = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, p.getNombre());
            pstmt.setLong(2, p.getId());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
	
	public long borrar(long numeroId) throws SQLException {

        String SQL = "DELETE FROM asignatura WHERE id = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, numeroId);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
	
	public Asignatura seleccionarPorId(long numeroId) {
		String SQL = "SELECT id, nombre FROM asignatura WHERE id = ? ";
		
		Asignatura p = null;
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, numeroId);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		p = new Asignatura();
        		p.setId(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return p;

	}
	
	
}

